package graphicalGame

import java.awt.Color
import java.awt.Graphics2D
import java.awt.geom.Ellipse2D
import java.rmi.server.UnicastRemoteObject

@remote trait RemotePlayer {

  def up: Unit
  def down: Unit
  def left: Unit
  def right: Unit
  def getx: Int
  def gety: Int
}

/**
 * @author cusiri
 */

//THis needs to tell the client who tells the serve what the player can do

class Player(initalX: Int, initalY: Int, val client: RemoteClient) extends UnicastRemoteObject with Entity with RemotePlayer {
  private var mLevel: Level = null
  //  private var posx = initalX
  //  private var posy = initalY
  val boxY = 700 / 15
  val boxX = 700 / 15
  def x = mx
  def y = my
  protected var mx = initalX
  protected var my = initalY
  private var upPressed = false
  private var downPressed = false
  private var leftPressed = false
  private var rightPressed = false

  override def update(): Unit = {

    //TODO

    var nx = mx
    var ny = my
    //    if (upPressed) println("In update " + up + " " + this)
    if (upPressed) ny -= 1
    if (downPressed) ny += 1
    if (leftPressed) nx -= 1
    if (rightPressed) nx += 1
    if (nx >= 0 && nx < level.maze.size && ny >= 0 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
      mx = nx
      my = ny
    }
  }

  //  def level = mLevel
  //  def level_=(l: Level): Unit = mLevel = l

  def drawPlayer(g: Graphics2D) {
    g.setPaint(Color.yellow)
    g.fill(new Ellipse2D.Double(mx * boxY, my * boxX, 700 / 15, 700 / 15)) // (posx,posy,widthP,lengthPlayer)
  }

  def move(): Unit = {
    val nx = mx - 1
    val ny = my - 1
    if (nx >= -1 && nx < level.maze.size && ny >= -1 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
      mx = nx
      my = ny
    }
  }

  def up(): Unit = {
    upPressed = true
    val nx = mx
    val ny = my - 1
    if (nx >= -1 && nx < level.maze.size && ny >= -1 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
      my = ny
    }
  }

  def down(): Unit = {
    downPressed = true
    val nx = mx
    val ny = my + 1
    if (nx >= -1 && nx < level.maze.size && ny >= -1 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
      my = ny
    }
  }

  def left(): Unit = {
    leftPressed = true
    val nx = mx - 1
    val ny = my
    if (nx >= -1 && nx < level.maze.size && ny >= -1 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
      mx = nx
    }
  }

  def right(): Unit = {
    rightPressed = true
    val nx = mx + 1
    val ny = my
    if (nx >= -1 && nx < level.maze.size && ny >= -1 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
      mx = nx
    }
  }

  def killedBy(enemies: List[Crabs]): Boolean = {
    enemies(0).hits(mx, my) //enemy is in current position of player
  }
  def makePassable: PassableEntity = new PassableEntity(mx, my, Entity.playerValue)

}