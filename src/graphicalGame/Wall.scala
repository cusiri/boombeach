package graphicalGame

/**
 * @author cusiri
 */
object Wall extends MapElement {
  def canPass: Boolean = false
}