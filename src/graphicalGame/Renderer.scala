package graphicalGame

import java.awt.Color

import java.awt.Graphics2D
import java.awt.geom.Ellipse2D
import java.awt.geom.Rectangle2D

/**
 * @author cusiri
 */

class Renderer {
  def render(g: Graphics2D, level: PassableLevel, width: Double, height: Double): Unit = {
    val boxWidth = width.toDouble / level.maze.length
    val boxHeight = height.toDouble / level.maze(0).length
    for (i <- level.maze.indices; j <- level.maze(i).indices) {
      level.maze(i)(j) match {
        case Wall => g.setPaint(Color.black)
        case Floor => g.setPaint(Color.yellow)

      }
      g.fill(new Rectangle2D.Double(j * boxWidth, i * boxHeight, boxWidth, boxHeight))
    }

    for (e <- level.characters) {
      e.etype match {
        case Entity.enemyValue => g.setPaint(Color.red)
        case Entity.playerValue => g.setPaint(Color.blue)

      }
//      g.setPaint(Color.red)
      g.fill(new Ellipse2D.Double(e.x * boxWidth, e.y * boxHeight, boxWidth, boxHeight))

    }

    /* for (e <- level.users) { // was e <- level.users then changed to character
      g.setPaint(Color.blue)
      g.fill(new Ellipse2D.Double(e.x * boxWidth, e.y * boxHeight, boxWidth, boxHeight))
    }*/
    /*   for(e <- level.multiplePlayers) {
      g.setPaint(Color.yellow)
      g.fill(new Ellipse2D.Double(e.x*boxWidth,e.y*boxHeight,boxWidth,boxHeight))
    } */
  }
}