package graphicalGame

import java.awt.Dimension

import java.awt.Graphics2D
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.rmi.Naming
import java.rmi.registry.LocateRegistry
import scala.swing._
import scala.swing.MainFrame
import scala.swing.Panel
import scala.swing.Swing
import scala.swing.event.Key
import scala.swing.event.KeyPressed
import java.rmi.Remote
import javax.swing.Timer
import java.rmi.server.UnicastRemoteObject

@remote trait RemoteSever {
  def joinGame(client: RemoteClient): RemotePlayer

}
/**
 * @author cusiri
 */

object Main extends UnicastRemoteObject with RemoteSever {
  println("Making server")
  LocateRegistry.createRegistry(1099)
  Naming.rebind("GameServer", this)

  val maze = Array(Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
    Array(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
    Array(1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
    Array(1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
    Array(1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1),
    Array(1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1),
    Array(1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1),
    Array(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
    Array(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1),
    Array(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1),
    Array(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1),
    Array(1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1),
    Array(1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1),
    Array(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1),
    Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1))

  val mazeLength = 700
  val mazeWidth = 700
  val initalX = 8
  val initalY = 7
  //val player = List(new Player(3, 3))
  

//  val enemies = List(new Crabs(2, 1), new Crabs(3, 2), new Crabs(4, 1), new Crabs(5, 8), new Crabs(6, 8), new Crabs(7, 8), new Crabs(8, 8))
  val level = new Level(maze, List[Entity](new Crabs(2, 1), new Crabs(7, 2), new Crabs(4, 7), new Crabs(10, 10), new Crabs(6, 8), new Crabs(7, 8), new Crabs(8, 8)))

  //It is the wrong list you need to use the list that is in the level 

  //println(level.depthFirstShortestPath(0, 0, 9, 9))
  println(level.bredthFirstShortestPath(1, 1, 9, 9))
//  enemies.foreach(_.level = level)
//  Players.foreach(_.level = level)
  // Needs to be passableLevel

  def joinGame(client: RemoteClient): RemotePlayer = {
    val p = new Player(1, 1, client)
//    Players ::= p
    level.addEntity(p)
    
    p: RemotePlayer

  }

  //  val panel = new Panel {
  //    override def paint(g: Graphics2D) = {
  //      // Renderer.render(g, level, mazeWidth, mazeLength)
  //    }
  //    preferredSize = new Dimension(mazeLength, mazeWidth)
  //
  //    listenTo(keys)
  //    reactions += {
  //      case e: KeyPressed =>
  //        if (e.key == Key.Left) players(0).left
  //        else if (e.key == Key.Right) players(0).right
  //        else if (e.key == Key.Up) players(0).up
  //        else if (e.key == Key.Down) players(0).down
  //        /*            if(e.key==Key.A) player2.left
  //             if(e.key==Key.D) player2.right
  //             if(e.key==Key.W) player2.up
  //             if(e.key==Key.F) player2.down  */
  //        repaint
  //    }
  //  }

  val timer = new Timer(300, Swing.ActionListener { ae =>
//    enemies.foreach(_.update)
    level.updateAll
    level.players.foreach(_.client.updateLevel(level.buildPassable))
    // panel.repaint()
  })

  //make enemy follow player
  //  enemies.move(player.posx,player.posy) 

  //end game
  /* if(player == player(0).killedBy(enemies)){
        println("Game Over")
        sys.exit(0) 
          
  } */

  val drawPanel = new Panel {
    override def paint(g: Graphics2D): Unit = {
      //Renderer.render(g, level, size.width, size.height)
    }
    preferredSize = new Dimension(800, 600)
  }
  def load: Unit = {
    val chooser = new FileChooser
    if (chooser.showOpenDialog(drawPanel) == FileChooser.Result.Approve) {
      val ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(chooser.selectedFile)))
      ois.readObject() match {
        case l: Level =>
          val level = l
          drawPanel.repaint()
        case _ => println("Read something that wasn't a level.")
      }
      ois.close()
    }
  }
  def save: Unit = {
    val chooser = new FileChooser
    if (chooser.showSaveDialog(drawPanel) == FileChooser.Result.Approve) {
      val oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(chooser.selectedFile)))
      oos.writeObject(level)
      oos.close()
    }
  }
  def main(args: Array[String]) {
    timer.start()

    //    val frame = new MainFrame {
    //      title = "The BoomBeach"
    //      contents = panel
    //      centerOnScreen
    //
    //      menuBar = new MenuBar {
    //        contents += new Menu("File") {
    //          contents += new MenuItem(Action("Pause")(timer.stop()))
    //          contents += new MenuItem(Action("Resume")(timer.start()))
    //          contents += new MenuItem(Action("Save")(save))
    //          contents += new MenuItem(Action("Load")(load))
    //        }
    //      }
    //    }

    // frame.open
    // panel.requestFocus

  }
}