package graphicalGame

/**
 * @author cusiri
 */
trait MapElement extends Serializable {
  def canPass: Boolean
}