package graphicalGame

/**
 * @author cusiri
 */
class Level(intMaze: Array[Array[Int]], private var chars: List[Entity]) {
  val maze = intMaze.map(row => row.map(_ match {
    case 0 => Floor
    case 1 => Wall

  }))
  chars.foreach(_.level = this)

  def characters = chars

  def addEntity(e: Entity): Unit = {
    chars ::= e
    e.level = this

  }

  def players = chars.collect { case p: Player => p } // This takes a partial function, gets out list of player. Tells main about the list in Level

  def removeEntity(victims: Crabs): Unit = {
    chars = chars.filter(_ != victims)

  }
  def depthFirstShortestPath(x: Int, y: Int, ex: Int, ey: Int): Int = {
    if (x == ex && ex == ey) 0
    else if (x < 0 || y < 0 || x > maze.length || y > maze.length || !maze(x)(y).canPass) 10000000
    else {
      val tmp = maze(x)(y)
      val ret = (depthFirstShortestPath(x - 1, y, ex, ey) min
        depthFirstShortestPath(x + 1, y, ex, ey) min
        depthFirstShortestPath(x, y - 1, ex, ey) min
        depthFirstShortestPath(x, y + 1, ex, ey)) + 1
      maze(x)(y) = tmp
      ret

    }

  }
  def bredthFirstShortestPath(y: Int, x: Int, ey: Int, ex: Int): Int = {

    val queue = scala.collection.mutable.Queue[(Int, Int, Int)]()
    val visited = scala.collection.mutable.Set[(Int, Int)]()

    def happySpot(cx: Int, cy: Int): Boolean = {
      cx >= 0 && cy >= 0 && cx < maze.length && cy < maze(cx).length && maze(cx)(cy).canPass &&
        !visited((cx, cy))
    }
    if (happySpot(x, y)) queue.enqueue((x, y, 0))
    visited.add((x, y))
    while (queue.nonEmpty) {
      val (cx, cy, steps) = queue.dequeue()
      if (cx == ex && cy == ey) return steps
      if (happySpot(cx + 1, cy)) { queue.enqueue((cx + 1, cy, steps + 1)); visited += ((cx + 1, cy)) }
      if (happySpot(cx - 1, cy)) { queue.enqueue((cx - 1, cy, steps + 1)); visited += ((cx - 1, cy)) }
      if (happySpot(cx, cy + 1)) { queue.enqueue((cx, cy + 1, steps + 1)); visited += ((cx, cy + 1)) }
      if (happySpot(cx, cy - 1)) { queue.enqueue((cx, cy - 1, steps + 1)); visited += ((cx, cy - 1)) }
    }
    100000000
  }

  //Define a helper function that checks to see of the tile is 
  def updateAll: Unit = chars.foreach { _.update }

  def buildPassable: PassableLevel = {
    new PassableLevel(maze, chars.map(_.makePassable).toArray)

  }

}

//Passage level needs a maze, and information about the entities eg passage entity both need to be serializable 