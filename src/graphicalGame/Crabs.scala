package graphicalGame

/**
 * @author cusiri
 */

//This is my Entity Trait

class Crabs(initPosX: Int, initPosY: Int) extends Entity {
  //  private var mLevel: List[Player] = player
  protected var mx = initPosX
  protected var my = initPosY

  def x = mx
  def y = my

  val size = math.Pi * (800 / 30 * 800 / 30) //area of circle

  /*def move(endX:Int,endY:Int) { //make enemy chase player
      val dx = endX - initPosX
      val dy = endY - initPosY
      val len = math.sqrt(dx*dx+dy*dy)
      initPosX += dx/len
      initPosY += dy/len
  } */

  def hits(playerX: Double, playerY: Double): Boolean = { //support killedBy in player
    val dx = initPosX - playerX
    val dy = initPosX - playerY
    val dist = math.sqrt(dx * dx + dy * dy) //distance equation
    dist <= size //size = area of enemy & player   
  }
  //
  //  def level = mLevel
  //
  //  def level_=(l: Level): Unit = mLevel = l //what is this???

  def update(): Unit = {

    var track = 10000.0
    var distP: Player = null
    for (p <- level.players) {

      val dx = p.x - x
      val dy = p.y - y
      val dist = math.sqrt(dx * dx + dy * dy)
      //TODO find the closest player to enemies.
      if (dist < track) {
        track = dist
        distP = p
      }

    }
    println(distP)
    if (distP == null) return

    //    level.depthFirstShortestPath(x + 1, y, distP.x, p.y)
    //    level.depthFirstShortestPath(x - 1, y, p.x, p.y)
    //    level.depthFirstShortestPath(x, y + 1, p.x, p.y)
    //    level.depthFirstShortestPath(x, y - 1, p.x, p.y)
    val rightdist = level.bredthFirstShortestPath(x + 1, y, distP.x, distP.y)
    val leftdist = level.bredthFirstShortestPath(x - 1, y, distP.x, distP.y)
    val upDist = level.bredthFirstShortestPath(x, y - 1, distP.x, distP.y)
    val downDist = level.bredthFirstShortestPath(x, y + 1, distP.x, distP.y)
    val short = List(rightdist, leftdist, upDist, downDist).min
    if (short < 1000000) {
      var nx = mx //randomly move to a location
      var ny = my
      if (rightdist == short) {
        nx += 1
      } else if (leftdist == short) {
        nx -= 1
      } else if (upDist == short) {
        ny -= 1
      } else if (downDist == short) {
        ny += 1
      }

//      if (nx >= 0 && nx < level.maze.size && ny >= 0 && ny < level.maze(0).size && level.maze(ny.toInt)(nx.toInt).canPass) {
        mx = nx
        my = ny
        //get player current position than got +1 -1 -maze.length + maze.length to avoid diagonal
//      }

    }

    println(rightdist)
    println(leftdist)
    println(upDist)
    println(downDist)

  }

  def makePassable: PassableEntity = new PassableEntity(mx, my, Entity.enemyValue)
}

object Crabs {

}