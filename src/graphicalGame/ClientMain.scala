package graphicalGame

import java.rmi.Remote



import java.rmi.RemoteException
import java.rmi.server.UnicastRemoteObject
import scala.collection.mutable.Buffer
import collection.mutable
import java.awt.Color
import java.awt.Graphics2D
import java.awt.geom.Ellipse2D
import java.awt.geom.Rectangle2D
import scala.swing.MainFrame
import scala.swing.Panel
import scala.swing.Swing
import scala.swing.event.KeyPressed
import java.util.Timer
import java.awt.Dimension
import scala.swing.event.Key

import java.awt.image.BufferedImage
import java.rmi.Naming
import scala.swing.Component
import scala.swing._

@remote trait RemoteClient {
  def updateLevel(l: PassableLevel)

}
/**
 * @author cusiri
 */
// Movement should go to client, then sends command to server who sends to everyone else, then same method is called on other objects

object ClientMain extends UnicastRemoteObject with RemoteClient {

  val server = Naming.lookup("rmi://localhost/GameServer") /* This takes the machine name*/ match {
    case s: RemoteSever => s
    case _ => throw new RuntimeException("Invalid type for RMI Server")

  }

  //The Renderer is the main thing that goes into the client. 
  //The Client draws things for user to see and send users input back into server
  println(server)

  val player = server.joinGame(this)
  println(player)
  //val playerNumber = server.connect(this)
  // println(playerNumber) // Allows players to connect to server, we made this in the server main ATM returns 0

  private var level: PassableLevel = null
  val renderer = new Renderer

  val panel2 = new Panel {
    override def paint(g: Graphics2D): Unit = {
      if (level != null) renderer.render(g, level, size.width, size.height)

    }

    listenTo(keys)
    reactions += {
      case e: KeyPressed =>
        if (e.key == Key.Left) player.left
        if (e.key == Key.Right) player.right
        if (e.key == Key.Up) player.up
        if (e.key == Key.Down) player.down
        /*            if(e.key==Key.A) player2.left
             if(e.key==Key.D) player2.right
             if(e.key==Key.W) player2.up
             if(e.key==Key.F) player2.down  */
        repaint
    }
    preferredSize = new Dimension(800, 600)

  }
  def updateLevel(l: PassableLevel): Unit = {
    level = l
    if (panel2 != null) panel2.repaint()

  }

  val frame = new MainFrame {
    title = "The BoomBeach"
    contents = panel2
    centerOnScreen

  }
  def main(args: Array[String]) = {
    frame.open
    panel2.requestFocus

  }

}