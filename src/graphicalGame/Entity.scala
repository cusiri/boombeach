package graphicalGame

/**
 * @author cusiri
 */
trait Entity extends Serializable {
  protected var mx: Int
  protected var my: Int

  private var mLevel: Level = null

  def getx = mx

  def gety = my

  def level = mLevel

  def level_=(l: Level): Unit = mLevel = l

  def update(): Unit

  def makePassable: PassableEntity
}

object Entity {
  val enemyValue = 0

  val playerValue = 2
}